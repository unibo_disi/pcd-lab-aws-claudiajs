const Api = require('claudia-api-builder');

const api = new Api();
api.get('/', () => 'Welcome to my API');
api.get('/hello/{x}', (req) => { return "hello " + req.pathParams.x; });

module.exports = api; // export your Claudia API Builder instance
