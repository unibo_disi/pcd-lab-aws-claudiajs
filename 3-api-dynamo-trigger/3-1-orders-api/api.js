const AWS = require('aws-sdk');
const uuid = require('uuid');
const Api = require('claudia-api-builder');

const api = new Api();
api.get('/', () => 'Welcome to Orders API');
api.put('/orders', (req) => createOrder(req.body));

const docClient = new AWS.DynamoDB.DocumentClient();

function createOrder(req) {
  if (!req || !req.product) throw new Error('Invalid request: no product found');
  return docClient.put({ TableName: 'orders',
    Item: { orderId: uuid(), product: req.product, orderStatus: 'pending' }
  })
  .promise()
  .then((res) => { console.log('Order saved in DynamoDB: ',res); return res; })
  .catch((err) => { console.log('Error while putting order in DynamoDB: ',err); throw err; } );
};

module.exports = api;
