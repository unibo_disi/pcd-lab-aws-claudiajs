const AWS = require('aws-sdk');
let lambda = new AWS.Lambda();

exports.handler = function (event, ctx, callback) {
  let req = lambda.getAccountSettings();
  req.promise().then(
      (data) => {
          console.log("Function: ", ctx.functionName, ctx.functionVersion);
          console.log("Event data: ", JSON.stringify(event));
          console.log("Request: ", ctx.awsRequestId);
          console.log("Remaining time: ", ctx.getRemainingTimeInMillis());
          callback(null, data);
      },
      (err) => {
          console.log(err);
          callback(err);
      }
  );
};
