#!/usr/bin/env bash
set -x #echo on

if [ "$#" -ne 1 ]; then
  export NAME="$(cat package.json | jp 'name' | tr -d '\"')"
else
  export NAME="$1"
fi

aws iam delete-role-policy --role-name $NAME-executor --policy-name log-writer
aws iam delete-role --role-name $NAME-executor
aws lambda delete-function --function-name $NAME
