exports.handler = function (event, ctx, callback) {
  console.log("Function: ", ctx.functionName, ctx.functionVersion);
  console.log("Event data: ", JSON.stringify(event));
  console.log("Request: ", ctx.awsRequestId);
  console.log("Remaining time: ", ctx.getRemainingTimeInMillis());
  callback(null,'hello world 2');
  //ctx.succeed('hello world');
  // callback();     // success but no info returned to the caller
  // callback(null); // success but no info returned to the caller
  // callback(null, "success");  // success with info returned to the caller
  // callback(err); // error with error info returned to the caller.
};
