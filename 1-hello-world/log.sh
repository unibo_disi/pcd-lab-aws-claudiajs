#!/usr/bin/env bash
set -x #echo on

if [ "$#" -ne 1 ]; then
  export NAME="$(cat package.json | jp 'name' | tr -d '\"')"
else
  export NAME="$1"
fi

aws logs filter-log-events --log-group-name /aws/lambda/$NAME --region us-east-1 | grep "message"

